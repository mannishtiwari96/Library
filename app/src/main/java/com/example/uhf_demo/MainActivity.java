package com.example.uhf_demo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import com.speedata.libuhf.IUHFService;
import com.speedata.libuhf.UHFManager;
import com.speedata.libuhf.bean.SpdInventoryData;
import com.speedata.libuhf.interfaces.OnSpdInventoryListener;


public class MainActivity extends AppCompatActivity  {
    IUHFService iuhfService;
    Button Start, stop, SetRange;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iuhfService = UHFManager.getUHFService(this);
//
//        openDev();
        Start = findViewById(R.id.button_Start);
        stop = findViewById(R.id.button_Stop);
        SetRange = findViewById(R.id.button_SetRange);
        Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        iuhfService.setOnInventoryListener(new OnSpdInventoryListener() {
            @Override
            public void getInventoryData(SpdInventoryData var1) {
                iuhfService.openDev();
                Toast.makeText(MainActivity.this, "STATE Starting....", Toast.LENGTH_SHORT).show();
            }
        });
//                Toast.makeText(MainActivity.this, "Start... "+num, Toast.LENGTH_SHORT).show();
            }
        });
    }
//    private boolean openDev() {
//        if (iuhfService.openDev() != 0) {
//            Toast.makeText(MainActivity.this, "Working...", Toast.LENGTH_SHORT).show();
//        }
//        return false;
//    }

   }